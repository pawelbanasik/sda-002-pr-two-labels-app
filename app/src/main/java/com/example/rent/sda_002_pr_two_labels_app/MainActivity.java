package com.example.rent.sda_002_pr_two_labels_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textViewOne;
    private TextView textViewTwo;
    private Button clickable_button;
    private int clickCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewOne = (TextView) findViewById(R.id.text_view_one);
        textViewTwo = (TextView) findViewById(R.id.text_view_two);

        clickable_button = (Button) findViewById(R.id.button_one);
    }

    // zrobione bez ButterKnife
    public void buttonClickMethod(View v) {

        if (clickCounter == 0) {
            textViewOne.setText("Pierwsze klikniecie");
        } else if (clickCounter == 1) {
            textViewTwo.setText("Drugie klikniecie");
        }
        clickCounter++;


    }

}
